## Intro
In-depth look how the menu of `Phoenix AwardBIOS v6.00PG` is stored.

## Structure
Thanks to awesome job by [Michael Tedder](https://sourceforge.net/projects/awdbedit/) we start with such stucture:
```
 typedef struct
 {
	WORD	status;                 - disabled, removed, padding, etc.
	WORD	pointerToHeader;        - refers to string in _EN_CODE.BIN
	WORD	chipRegIndex;
	WORD	chipRegMask;
	BYTE	cmosIndex;              - sometimes with 7th bit set (uses different i/o function)
	WORD	cmosMask;
	WORD	pointerToItemTable;     - selectable items if it's "listbox"
	WORD	itemMinIndex;
	WORD	itemMaxIndex;
	BYTE	xPosition;
	BYTE	yPosition;
	WORD	biosDefaultIdx;
	WORD	setupDefaultIdx;
	WORD	pointerToHelp;
 } sysbiosMenuStruct;
```
It saved me a lot of time but also caused too many headaches and frustrations. It's only natural that you accept work of someone much more skilled with absolute certanity. But this was my mistake. While it works fine for `v4.00` it's not true for `v6.00PG`. Let me explain.
```
	BYTE	xPosition;
	BYTE	yPosition;
```
After briefly checking few structures I've noticed that these values are off but I was under firm impression they're somehow updated later by the code. Menu is dynamic, so there was no point in storing fixed coordinates, right? But then again, I couldn't find any other structure that'd contain what you see on the screen. After many hours of disassembling and reverse engeneering I figured out it's not what it is. It should be this instead:
```
	WORD	order;
```
where all 16 bits have different meaning:
```
15-11:    0 standard entry, 1-31 submenu (group id)
10- 5:    1-63 entry's order it's being displayed with
 0- 4:    group id entry belongs to
```
And sure enough, this is the result:
```
  [04](02)(08) Advanced Chipset Features	    (value: 2048h)
	  [25](09)(04) DRAM Clock/Timing Control	(value: C924h)
		 x[00](10)(25) Current CPU Frequency 	(value: 0159h)
		 x[00](10)(25) Current DRAM Frequency 	(value: 0159h)
		  [00](10)(25) DRAM Frequency	        (value: 0159h)
		  [00](10)(25) Auto Configuration	    (value: 0159h)
		  [00](10)(25) DDR SDRAM CAS Latency	(value: 0159h)
		  [00](10)(25) DDR Input Terminate Ctrl	(value: 0159h)
	  [30](09)(04) Advanced Host Control	    (value: F124h)
		  [00](10)(30) Memory Hole at 15M-16M	(value: 015Eh)
		  [00](10)(30) C1 State Disconnect	    (value: 015Eh)
		  [00](10)(30) C2 State Disconnect	    (value: 015Eh)
		  [00](10)(30) C3 State Disconnect	    (value: 015Eh)
(...)
```
What's interesting all main menu entries and submenus belong to group 8. If both entries have the same order value they're displayed in order their structures are stored in. To remove entry from menu 4th bit of `status` must be set. 3rd bit disables entry but leaves it on the screen - this is used for non selectable information. The other way to hide entries is to put their submenus into one group ie. 30 and not to have any other submenu with such id - this is what HP BIOS does.

## Unlocking
Knowing where menu structures are located and how subroutine handles them I wrote a simple dumper which extracts all the entries and also menu as it is presented. This way I got all the values I needed. Next step was to identify main submenus:
```
[04](02)(30) Advanced Chipset Features	(struct: 0322h, offset: 0333h, value: 205Eh)
```
and change their group id from 30 to 8. This put them back into menu. Identifying single entries is daunting task but thankfully [Futro S400](https://www.parkytowers.me.uk/thin/Futro/s400/) has similar hardware and unrestricted BIOS. So it was just dump and compare job. Some values could be copied straight others needed adjustments. For that purpose I wrote myself small patching utility that reads address/value pairs from text file and applies changes to binary. Then it was just usual stuff, rebuilding and flashing, to see results.

## Cleaning
While majority of options work and look good there're also some needless ones which can be omitted. I don't expect anyone to use ie. `Onboard FDC Controller` or `Current CPU FAN1 Speed`. There's no floppy connector and while fan connector needs to be populated (it's close to PCI slot) the option has no values to choose from. Also `PC Health Status` submenu was left disabled - no readings were updated (missing sensors or code?). As stated above best way is to set `4th` bit in appropriate structure `status` field. I also slightly updated entries order to have nice and clean structure.