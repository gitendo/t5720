## Intro
I'll describe the process of rebuilding custom `Phoenix AwardBIOS v6.00PG` below.

## Tools of trade
- AWDFLASH v8.83 (DOS)
- BIOS Patcher v4.23 (DOS) - this is optional, provides text dump of _EN_CODE.BIN if you're too lazy to disassemble it
- CBROM V2.07 (DOS)
- LHA v2.55 (DOS)
- CBROM v1.98 (Windows)
- Hex Editor
- C/C++ environment

## BIOS Structure
We deal with `Phoenix AwardBIOS v6.00PG` - 4Mbit image.
```
CBROM V2.07 (C)Award Software 2000 All Rights Reserved.

              ******** 240HV112.BIN BIOS component ********

 No. Item-Name         Original-Size   Compressed-Size Original-File-Name 
================================================================================
  0. System BIOS       20000h(128.00K)12B63h(74.85K)240hv112.BIN
  1. XGROUP CODE       0D200h(52.50K)089EDh(34.48K)awardext.rom
  2. ACPI table        0359Eh(13.40K)01510h(5.27K)ACPITBL.BIN
  3. YGROUP ROM        09DB0h(39.42K)04A23h(18.53K)awardeyt.rom
  4. GROUP ROM[ 0]     04820h(18.03K)02020h(8.03K)_EN_CODE.BIN
  5. LOGO BitMap       2443Ch(145.06K)00994h(2.39K)hp75.bmp
  6. VGA ROM[1]        08000h(32.00K)042F6h(16.74K)VGA23600.ROM
  7. PCI driver[A]     0A800h(42.00K)062F2h(24.74K)PXEB.LOM
  8. OEM0 CODE         00D0Fh(3.26K)0087Ch(2.12K)int15_32.bin
  9. Other(4045:0000)  007DAh(1.96K)00801h(2.00K)rom32.bin

  Total compress code space  = 4D000h(308.00K)
  Total compressed code size = 2F49Ch(189.15K)
  Remain compress code space = 1DB64h(118.85K)

 ****** On Board VGA ROM In BootBlock ******

                          ** Micro Code Information **
Update ID  CPUID  |  Update ID  CPUID  |  Update ID  CPUID  |  Update ID  CPUID
------------------+--------------------+--------------------+-------------------
```
 Main BIOS module aka `240hv112.BIN` is located at `0x20000`. It's compressed [LHA](https://github.com/jca02266/lha/blob/master/header.doc.md) archive followed by other modules that are easily spotted (try searching for `-lh5-` and `-lh0-` tags). Non relocateable parts are at `0x6D000` - this is something related to decompression, on board VGA ROM and fonts. There's a boot block at `0x7E000`.

## Tinkering
Main BIOS module can be easily extracted with 7-Zip while opening BIOS file as archive. As for modules, use CBROM for Windows to extract all except `rom32.bin`. This one should be extracted with DOS version to get vaild file. Here's batch file:
```
@ECHO OFF
SET file=240HV112.BIN
CBROM.EXE %file% /XGROUP Extract
CBROM.EXE %file% /ACPI Extract
CBROM.EXE %file% /YGROUP Extract
CBROM.EXE %file% /GROUP Extract
CBROM.EXE %file% /LOGO Extract
CBROM.EXE %file% /VGA Extract
CBROM.EXE %file% /PCI Extract
CBROM.EXE %file% /OEM0 Extract
CBROM.EXE %file% /other 4045:0 Extract /ERR
```
This can also be achieved with hex editor if you prefer the hard way.

While CBROM (Windows) handles decompression/compression and inserting of the modules you're on your own when it comes to main module - `240hv112.BIN`. In the next step we'll remove all modules from the BIOS file. Here's batch file:
```
@ECHO OFF
SET file=240HV112.BIN
CBROM.EXE %file% /NoCompress Release
CBROM.EXE %file% /OEM0 Release
CBROM.EXE %file% /PCI Release
CBROM.EXE %file% /VGA Release
CBROM.EXE %file% /LOGO Release
CBROM.EXE %file% /GROUP Release
CBROM.EXE %file% /YGROUP Release
CBROM.EXE %file% /ACPI Release
CBROM.EXE %file% /XGROUP Release
CBROM.EXE %file% /D
```
Such file will serve as a base for further modifications. After applying changes to main module (`240hv112.BIN`) it needs to be compressed with LHA v2.55 for DOS, which creates 1:1 archive. [Header fields](https://github.com/jca02266/lha/blob/master/header.doc.md) responsible for `time` and `date` are used as `segment:offset` pair so they need to be restored. For sake of completeness you might also want to restore OS ID field and then recalculate header checksum to end up with valid archive (look for [lhapatcher.c](tools/lhapatcher.c) somewhere in this repository). Such file can be inserted back at `0x20000` overwriting previous content. Make sure there're no leftovers of previous version if updated file somehow is smaller. Then we reconstruct BIOS image:
```
@ECHO OFF
SET file=240HV112.BIN
CBROM.EXE %file% /XGROUP awardext.rom
CBROM.EXE %file% /ACPI ACPITBL.BIN
CBROM.EXE %file% /YGROUP awardeyt.rom
CBROM.EXE %file% /GROUP _EN_CODE.BIN
CBROM.EXE %file% /LOGO hp75.bmp
CBROM.EXE %file% /VGA VGA23600.ROM
CBROM.EXE %file% /PCI PXEB.LOM
CBROM.EXE %file% /OEM0 int15_32.bin
CBROM.EXE %file% /NoCompress rom32.bin
CBROM.EXE %file% /D
```
Output should be valid and accepted by `AWDFLASH`. While flashing I used original parameters: `awdflash.exe 240HV112.bin /py /sn /cp /sb` that prevents overwriting boot block - some sort of security measure I guess.

## Findings
- `_EN_CODE.BIN` contains all the options and descriptions visible in BIOS menu and more. There're some control codes like moving cursor, jumping to other string, etc. but nothing to enable / disable main menu entries. You can however enable some selectable options which end with `01` instead of `00` as other strings. The most important thing is its structure. There's main array of pointers to sub arrays that contain pointers to menu strings. To refer to a string you need two, byte sized indexes that are stored as a word.

- `awardext.rom` has the procedure that buids the menu. It's located at `0x4850`.
```
seg000:4850 sub_4850        proc near
seg000:4850                 push    ds
seg000:4851                 push    es
seg000:4852                 pushad
seg000:4854                 mov     ax, [bp+190h]
seg000:4858                 mov     ds, ax
seg000:485A                 mov     es, ax
seg000:485C                 xor     di, di
seg000:485E                 xor     ax, ax
seg000:4860                 cld
seg000:4861                 mov     cx, 8000h       ; zerofill whole segment
seg000:4864                 rep     stosw
seg000:4866                 call    sub_4930        ; copy $Spe?? arrays related to menu
seg000:4869                 mov     dl, 1
seg000:486B                 mov     di, 240h
seg000:486E
seg000:486E loc_486E:
seg000:486E                 cmp     dl, 20h
seg000:4871                 ja      short loc_48D0  ; jump if dl > 32
seg000:4873                 mov     si, 2213h       ; array of pointers
seg000:4873                                         ; F000:2213 in 240hv112.bin
seg000:4876                 push    di
(...)
```

- `240hv112.BIN` unsurprisingly contains a lot of interesting stuff but most important are bios menu structures which refer to strings from `_EN_CODE.BIN`. While there's plenty of them, the most important array is located at `0x12213`:
```
seg001:2213 off_F2213       dw offset word_F0309    ; Advanced BIOS Features
seg001:2215                 dw offset aPressEnter_0 ; "Press Enter"
seg001:2217                 dw 0
seg001:2219                 dw offset word_F2968    ; 'Date (mm:dd:yy)'
seg001:221B                 dw offset asc_F2C6F     ; "Enabled "
seg001:221D                 dw 0
seg001:221F                 dw offset unk_F0F08     ; DRAM Clock/Timing Control
seg001:2221                 dw offset loc_F12D7
seg001:2223                 dw 0
seg001:2225                 dw offset unk_F1442     ; 'ACPI function'
seg001:2227                 dw offset aDisable      ; "Disable    "
seg001:2229                 dw 0
seg001:222B                 dw offset unk_F183C     ; 'Reset Configuration Data'
seg001:222D                 dw offset off_F1981
seg001:222F                 dw 0
seg001:2231                 dw offset unk_F19F9     ; 'SIS OnChip IDE Device'
seg001:2233                 dw offset unk_F1CB5
seg001:2235                 dw 0
seg001:2237                 dw offset word_F1D72    ; 'System Information'
seg001:2239                 dw offset loc_F1FCA
seg001:223B                 dw 0
(...)
```
How about some pointers to functions that handle control codes from `_EN_CODE.BIN`? Check out an array at `0x1314F`:
```
seg001:314F off_F314F       dw offset nullsub_1     ; pointers to procedures that create bios screen
seg001:3151                 dw offset nullsub_1     ; n/a
seg001:3153                 dw offset sub_F317B     ; 02h: create screen borders / window (+6)
seg001:3155                 dw offset sub_F32BA     ; 03h: clear part or whole screen (+4) via int 10h
seg001:3157                 dw offset sub_F32D0     ; 04h: print string / manage other codes (+5)
seg001:3159                 dw offset sub_F3342     ; 05h: set cursor x/y (+2)
seg001:315B                 dw offset sub_F3363     ; 06h: set highlight attribute
seg001:315D                 dw offset sub_F3374     ; 07h: set reverse attribute
seg001:315F                 dw offset sub_F337E     ; 08h: set normal attribute
seg001:3161                 dw offset sub_F33BC     ; 09h: set blink attribute
seg001:3163                 dw offset sub_F2FCB     ; 0Ah:
seg001:3165                 dw offset sub_F33CC     ; 0Bh:
seg001:3167                 dw offset sub_F33FE     ; 0Ch:
seg001:3169                 dw offset sub_F2FE8     ; 0Dh:
seg001:316B                 dw offset sub_F340A     ; 0Eh: add to cursor Y (+1)
seg001:316D                 dw offset sub_F3410     ; 0Fh: subtract from cursor Y (+1)
seg001:316F                 dw offset sub_F3416     ; 10h: call to [si] (+2)
seg001:3171                 dw offset sub_F341E     ; 11h: ??? (+1)
seg001:3173                 dw offset sub_F33C2     ; 12h: set warning attribute
seg001:3175                 dw offset sub_F3404     ; 13h: subtract [si] from cursor X (+1)
seg001:3177                 dw offset sub_F32F7     ; 14h: load si with [si]
seg001:3179                 dw offset sub_F32FA     ; 15h: print string from main/sub indexes (+2)
```
Another huge block of pointers to different functions is located at `0x564B`. There's stuff to check CMOS checksum, set default time and many more I didn't bother to figure out.

Let's continue to [MENU](MENU.md) where I'll connect all the dots together.