## Intro
This repository contains information, custom utilities and BIOS for HP T5720. You're free to use it on your own risk. There's no warranty of any kind. If you use my work put credit where it's due together with url to this repository.

Back in the days I was lucky enough to buy one of these together with expansion bay. Plugged it a few times, tinkered with SNES emulation, tried with nVidia Quadro NVS 280, expanded memory to 1GB did some other stuff and put it in the closet for years. Recently I've given it a second chance by fitting 2.5" HDD inside and PCI sound card. It's nice DOS, Windows 98SE / XP multipurpose box. The only drawback is BIOS. For some reason HP severly limited available options.

## Challenge
Enable options responsible for `VGA Share Memory Size` and `IRQ Resources` at least, anything useful at most.

## Status
Completed! Sucessfully restored all available options and updated VGA ROM to the latest revision. 

- `bios` directory contains original BIOS files and my unofficial update.
- `t5720vga` directory contains small utility to configure memory available for integrated graphics processor - if you prefer not to update BIOS.
- [custom](CUSTOM.md) contains process of rebuilding `Phoenix AwardBIOS v6.00PG`
- [menu](MENU.md) contains detailed information about the BIOS menu

## Proof

![Custom BIOS version](img/ver.jpg "v1.13")
![Custom BIOS menu](img/main.jpg "v1.13 menu")
![Custom BIOS info](img/info.jpg "v1.13 info")