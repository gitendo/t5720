Original HP t5720 BIOS files:
- v1.03 dumped from thin client
- v1.09 extracted from SoftPaq [sp34784.exe](https://www.google.com/search?q=HP+t5720+%22sp34784.exe%22)
- v1.12 extracted from SoftPaq [sp37648.exe](https://www.google.com/search?q=HP+t5720+%22sp37648.exe%22)

Custom HP t5720 BIOS file:
- v1.13 all BIOS menus enabled, VGA ROM updated from v2.36 to 2.53.12

:warning: Usual terms apply - you flash on your own risk and I'm not responsible for any failures. 

Use the recommended set of options: `awdflash.exe 240HV1xx.BIN /py /sn /cp /sb` while flashing.