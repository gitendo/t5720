﻿#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define VERSION 		1.0

int main (int argc, char *argv[])
{
    FILE *pFile, *pPatch;
    int bufferLength = 80, line;
    char buffer[bufferLength], *failed, *ptr, *token, type;
    unsigned int offset, value;

    printf("EasyPatcher v%.2f - text driven binary patcher\n", VERSION);
    printf("Programmed by: tmk, e-mail: tmk@tuta.io\n\n");

    switch(argc) {
        case 3:
            pFile = fopen(argv[1], "rb+");
        	if (!pFile) {
        		printf("Error: unable to access %s\n", argv[1]);
                exit(EXIT_FAILURE);
            }
    
            pPatch = fopen(argv[2], "r");
        	if (!pPatch) {
        		printf("Error: unable to access %s\n", argv[2]);
                fclose(pFile);
                exit(EXIT_FAILURE);
            }
            // read patch file, line by line
            printf("Patching %s\n\n", argv[1]);
            line = 1;
            while(fgets(buffer, bufferLength, pPatch)) {

                //skip comments
                ptr = strchr(buffer, ';');
                if(ptr != NULL) {
                    memset(buffer, 0, bufferLength);
                    line++;
                    continue;
                }

                // remove new line terminator
                ptr = strchr(buffer, '\n');
                if(ptr != NULL)
                    *ptr = 0;

                // get offset
                token = strtok(buffer, ":");
                if(strlen(token) > 8) {
                    printf("Error: invalid offset size in line %d\n", line);
                    break;
                }
                offset = strtoul(token, &failed, 16);
                if(*failed) {
                    printf("Error: invalid offset in line %d : %x(%s)", line, offset, failed);
                    break;
                }

                // get data type
                token = strtok(NULL, ":");
                if(token == NULL) {
                    printf("Error: syntax error detected in line %d\n", line);
                    break;
                }
                if(*token != 'b' && *token != 'w' && *token != 'd') {
                    printf("Error: invalid value type in line %d: %c\n", line, *token);
                    break;
                }
                type = tolower(*token);

                // get value
                token = strtok(NULL, ":");
                if(token == NULL) {
                    printf("Error: syntax error detected in line %d\n", line);
                    break;
                }
                if(strlen(token) > 8) {
                    printf("Error: invalid value size in line %d\n", line);
                    break;
                }
                value = strtoul(token, &failed, 16);
                if(*failed) {
                    printf("Error: invalid offset in line %d : %s", line, failed);
                    break;
                }

                // empty buffer
                memset(buffer, 0, bufferLength);
                line++;

                // move file pointer
                if(fseek(pFile, offset, SEEK_SET)) {
                    printf("Error: %s\n", strerror(errno));
                    break;
                }

                printf("0x%0.8x: ", offset);
                // patch
                switch(type) {
                    case 'b':
                        fwrite(&value, 1, 1, pFile);
                        printf("%0.2x\n", value);
                        break;
                    case 'w':
                        fwrite(&value, sizeof(short int), 1, pFile);
                        printf("%0.4x\n", value);
                        break;
                    case 'd':
                        fwrite(&value, sizeof(int), 1, pFile);
                        printf("%0.8x\n", value);
                        break;
                }
            }

            if(line == 0)
                printf("Error: patch file seems to be empty!");

            fclose(pFile);
            fclose(pPatch);
            break;

        default:
            printf("Usage:\teasypatcher.exe binary_file patch_file\n\n");
            printf("Patch file is expected to be plain text with:\n");
            printf("offset:length:value\n");
            printf("entry per each line. Offset and value must be hexadecimal without prefix or suffix.\n");
            printf("Accepted length code is (b)yte, (w)ord and (d)word.\n");
            printf("Comments line start with ; and cannot exceed 79 characters.\n");
            printf("Example: 45f04:w:56a8\n\n");
            break;
    }
}
