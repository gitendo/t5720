﻿#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#pragma pack(push, 1)

struct lha {
    unsigned char hdrsize;
    unsigned char hdrsum;
    char sig[5];
    int packed;
    int original;
    unsigned short int time;
    unsigned short int date;
    unsigned char attr;
    unsigned char level;
    unsigned char namelen;
    char name[12];
    unsigned short int crc;
    unsigned char osid;
};

#pragma pack(pop)

int main (int argc, char *argv[])
{
    struct lha      header;
    FILE			*fp;


    printf("240HV112.LZH header patcher...\n\n");

    fp = fopen(argv[1], "rb+");
	if (!fp)
    {
		printf("Input file not found!\n");
        exit(EXIT_FAILURE);
    }

    fread(&header, sizeof(header), 1, fp);

	fseek(fp, 0, SEEK_SET);

    if(strncmp(header.sig, "-lh5-", 5) != 0)
    {
		printf("Invalid header signature!\n");
        fclose(fp);
        exit(EXIT_FAILURE);
    }

    // restore original values
    header.time = 0x0000;
    header.date = 0x5000;
    strncpy(header.name, "240hv112.BIN", 12);
    header.osid = 0x20;

    unsigned char *ptr = (unsigned char *)&header;
    unsigned char chksum = 0;

    for(int i = 2; i < (int) sizeof(header); i++)
    {
        chksum += ptr[i];
    }

    header.hdrsum = chksum;

    printf("Header size: 0x%0.2X\n", header.hdrsize);
    printf("Header sum: 0x%0.2X\n", header.hdrsum);
    printf("Compressed size: %d\n", header.packed);
    printf("Original size: %d\n", header.original);
    printf("Time: 0x%0.4X\n", header.time);
    printf("Date: 0x%0.4X\n", header.date);
    printf("OS ID: 0x%0.2X\n", header.attr);

    fwrite(&header, sizeof(header), 1, fp);

    fclose(fp);

    printf("\nDone!\n");

}
