The following tools were created in the process. While they work please take into account that they're not idiot-proof nor polished.

- `easypatcher.c` - as the name suggests, used to apply patches from `240hv112.txt` to `240HV112.BIN`
- `lhapatch.c` - is to be used on `.LZH` archive of patched `240HV112.BIN` before it's inserted back into BIOS file
- `menudump.c` - dumps menu structure or all menu related entries, requires `240HV112.BIN` and `_EN_CODE.BIN`

:info: Whenever `240HV112.BIN` is mentioned I refer to main module not the BIOS file!