﻿#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAIN_MENU_ID 8
#define MENU_STRUCT  0x2213

unsigned char bios[65536];
unsigned char en_code[18464];

#pragma pack(push, 1)

struct menuEntry {
    //  1 - ??, 2 - ??, 4 - disabled, 8 - not displayed, 10h - empty line above, 20h - ??
	unsigned short int status;
	unsigned short int stringPtr;
	unsigned short int chipRegIndex;
	unsigned short int chipRegMask;
	unsigned char      cmosIndex;
	unsigned short int cmosMask;
	unsigned short int itemOptrionsPtr;
	unsigned short int itemMinIndex;
	unsigned short int itemMaxIndex;
	unsigned short int order;
	unsigned short int biosDefaultVal;
	unsigned short int setupDefaultVal;
	unsigned short int helpPtr;
};

#pragma pack(pop)

// dumps enabled and all disabled submenus and entries
void dumpAllEntries(void) {

    struct  menuEntry   *entry;
    unsigned short *ptrArray, *ptrStart, *ptrEnd, *ptrParent, *ptrChild;
    unsigned short tmp;
    unsigned char *ptrStr;
    int entry_gid, entry_number, entry_group, group, n;

    for(group = 0; group < 32; group++) {
        printf("\nGroup %0.2X:\n", group);
        for(n = 0; n < 64; n++) {
            ptrArray = (unsigned short *) &bios[MENU_STRUCT];
            // process array of pointers to bios menu string structures
            while(*ptrArray != 0xFFFF) {
                // pointer to first structure in block
                ptrStart = (unsigned short *) &bios[*ptrArray];
                ptrArray++;
                // pointer to where above block of structures ends
                ptrEnd = (unsigned short *) &bios[*ptrArray];
                ptrArray++;
                // let's access the structures easy way
                entry = (struct menuEntry *) ptrStart;
                // process each structure in given block
                while((unsigned short *) entry != ptrEnd) {
                    entry_gid = (entry->order & 0xF800) >> 11;
                    entry_number = (entry->order >> 5) & 0x3F;
                    entry_group = entry->order & 0x1F;
                    // look for entries from specified group and fetch them in ascending order
                    if(entry_group == group && entry_number == n) {
                        // prepare pointer to string text
                        tmp = ((entry->stringPtr & 0xFF00) >> 8) * 2;
                        ptrParent = (unsigned short *) &en_code[0x12+tmp];
                        tmp = (entry->stringPtr & 0x00FF) * 2;
                        ptrChild = (unsigned short *) &en_code[*ptrParent+2+tmp];
                        ptrStr = (unsigned char *) &en_code[*ptrChild];
                        printf("\t");
                        printf("%c", (entry->status & 8) ? '*' : ' ');
                        printf("%c", (entry->status & 4) ? 'x' : ' ');
                        printf("[%0.2d](%0.2d)(%0.2d) ", entry_gid, entry_number, entry_group);
                        // skip non printable characters, _EN_CODE happens to use some control codes
                        while(*ptrStr != '\0') {
                            printf("%c", isprint(*ptrStr) ? *ptrStr : ' ');
                            ptrStr++;
                        }
                        // debug info used for patching
                        int offset = (int)((size_t) entry - (size_t) bios);
                        printf("\t(struct: %0.4Xh, offset: %0.4Xh, value: %0.4Xh)\n", offset, offset + 0x11, entry->order);
                    }
                    // next structure
                    entry++;
                }
                // skip 0 entry
                ptrArray++;
            }
        }
        printf("\n");
    }
}


// dumps enabled submenus and entries only
void getEntries(int group) {

    struct  menuEntry   *entry;
    unsigned short *ptrArray, *ptrStart, *ptrEnd, *ptrParent, *ptrChild;
    unsigned short tmp;
    unsigned char *ptrStr, t;
    int entry_gid, entry_number, entry_group, n;
    static char tabs = 0;

    for(n = 0; n < 64; n++) {

        ptrArray = (unsigned short *) &bios[MENU_STRUCT];
        // process array of pointers to bios menu string structures
        while(*ptrArray != 0xFFFF) {
            // pointer to first structure in block
            ptrStart = (unsigned short *) &bios[*ptrArray];
            ptrArray++;
            // pointer to where above block of structures ends
            ptrEnd = (unsigned short *) &bios[*ptrArray];
            ptrArray++;
            // let's access the structures easy way
            entry = (struct menuEntry *) ptrStart;
            // process each structure in given block
            while((unsigned short *) entry != ptrEnd) {
                entry_gid = (entry->order & 0xF800) >> 11;
                entry_number = (entry->order >> 5) & 0x3F;
                entry_group = entry->order & 0x1F;
                // look for entries from specified group and fetch them in ascending order
                if(entry_group == group && entry_number == n) {                    
                    // prepare pointer to string text
                    tmp = ((entry->stringPtr & 0xFF00) >> 8) * 2;
                    ptrParent = (unsigned short *) &en_code[0x12+tmp];
                    tmp = (entry->stringPtr & 0x00FF) * 2;
                    ptrChild = (unsigned short *) &en_code[*ptrParent+2+tmp];
                    ptrStr = (unsigned char *) &en_code[*ptrChild];
                    // pretty print
                    if(group == MAIN_MENU_ID)
                            printf("\n");
                    for(t = 0; t < tabs; t++)
                        printf("\t");

                    printf("%c", (entry->status & 8) ? '*' : ' ');
                    printf("%c", (entry->status & 4) ? 'x' : ' ');
                    printf("[%0.2d](%0.2d)(%0.2d) ", entry_gid, entry_number, entry_group);
                    // skip non printable characters, _EN_CODE happens to use some control codes
                    while(*ptrStr != '\0') {
                        printf("%c", isprint(*ptrStr) ? *ptrStr : ' ');
                        ptrStr++;
                    }
                    // debug info used for patching
                    int offset = (int)((size_t) entry - (size_t) bios);
                    printf("\t(struct: %0.4Xh, offset: %0.4Xh, value: %0.4Xh)\n", offset, offset + 0x11, entry->order);
                    // if menu option has a grup fetch it recursively
                    if(entry_gid > 0) {
                        tabs++;
                        getEntries(entry_gid);
                        tabs--;
                    }
                }
                // next structure
                entry++;
            }
            // skip 0 entry
            ptrArray++;
        }
    }
}

int main (int argc, char *argv[])
{
    FILE    *fp = NULL;

    printf("Phoenix AwardBIOS v6.00PG menu dumper (0x%0.4x)\n", MENU_STRUCT);
    printf("Programmed by: tmk, e-mail: tmk@tuta.io\n");
    printf("Project page: https://gitlab.com/gitendo/t5720\n\n");

    fp = fopen("240hv112.BIN", "rb");
  	if (!fp) {
        printf("Error: unable to access 240hv112.BIN\n");
        exit(EXIT_FAILURE);
    }
	fseek(fp, 0x10000, SEEK_SET);
    fread(&bios, sizeof(bios), 1, fp);
    fclose(fp);

    fp = fopen("_EN_CODE.BIN", "rb");
  	if (!fp) {
        printf("Error: unable to access _EN_CODE.BIN\n");
        exit(EXIT_FAILURE);
    }
    fread(&en_code, sizeof(en_code), 1, fp);
    fclose(fp);

    printf("Press 'a' for all entries or 'm' for menu only. Enter to quit.\n");

    switch(getchar()) {
        case 'a':
            dumpAllEntries();
            break;
        case 'm':
            getEntries(MAIN_MENU_ID);
            break;
        default:
            break;
    }
}
